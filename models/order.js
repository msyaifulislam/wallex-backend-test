'use strict';
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('Order', {
      total: DataTypes.DECIMAL,
  });

  Model.associate = function(models){
      this.Users = this.belongsToMany(models.User, {through: 'UserOrder'});
  };




  Model.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Model;
};
