'use strict';
module.exports = (sequelize, DataTypes) => {
  var Model = sequelize.define('OrderDetail', {
      id_order: DataTypes.INTEGER,
      id_menu: DataTypes.INTEGER,
      total_item:DataTypes.INTEGER,
  });

  Model.prototype.toWeb = function (pw) {
      let json = this.toJSON();
      return json;
  };

  return Model;
};
