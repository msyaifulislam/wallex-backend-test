const Menu 			    = require('./../models').Menu;

let menu = async function (req, res, next) {
    let menu_id, err, menu;
    menu_id = req.params.menu_id;

    [err, menu] = await to(Menu.findOne({where:{id:menu_id}}));
    if(err) return ReE(res, "err finding menu");

    if(!menu) return ReE(res, "Menu not found with id: "+menu_id);

    let user, users_array, users;
    user = req.user;
    [err, users] = await to(menu.getUsers());

    users_array = users.map(obj=>String(obj.user));

    if(!users_array.includes(String(user._id))) return ReE(res, "User does not have permission to read app with id: "+app_id);

    req.menu = menu;
    next();
}
module.exports.menu = menu;
